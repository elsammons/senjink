package actions

import "time"

var (
	layout string = "2006-01-02"
)

/*
 CheckDateWithin startDate and endDate, will compare to inDate.
*/
func CheckDateWithin(inDate, startDate, endDate string) bool {

	check, _ := time.Parse(layout, inDate)
	start, _ := time.Parse(layout, startDate)
	end, _ := time.Parse(layout, endDate)

	return check.After(start) && check.Before(end.AddDate(0, 0, 1))
}

func CheckDateBefore(inDate, endDate string) bool {
	check, _ := time.Parse(layout, inDate)
	end, _ := time.Parse(layout, endDate)

	return check.Before(end)
}
