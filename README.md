# BACKGROUND #
My **first** attempt at a golang program.

# README #
A simple go program that queries a Jenkins server and reports on jobs, by search or jobnames.  Report is currently based on GetLastCompleteBuild().

## Prerequisite ##
* Golang Environment that is set up.
* bitbucket.org/elsammons/gojenkins
> **NOTE** 
> You must use this fork as it has exposed struct that is 
> not otherwise exposed in the base project.
## How do I get set up? ##
1. Download and unpack the project or use `go get`.
`go get bitbucket.org/elsammons/senjink`

> **NOTE**
> gojenkins will download and install automatically with `go get`

At this point senjink should be installed in your ${GOPATH}/bin

## Example Runs ##
> **NOTE**
> Be Sure you are in your ${GOPATH}/bin directory
> or that the directory is in your ${PATH}

`$ cd ${GOPATH}/bin; ./senjink --help`

```
#!

Usage of ./senjink:
  -bar int
    	The total number of tests that had to run to consider worth counting.
  -build int
    	A specific build you are interested in.
  -detail
    	Do you want a detailed view.
  -jobnames string
    	The specific job you are interested in
  -search string
    	Search for jobs that contain this string (default "libgdiplus")
  -url string
    	url of the jenkins server (default "https://jenkins.mono-project.com/")
```

`$ ./senjink -url=http://example.com:8080 -search=[something in the jobname]`
```
#!
========================
Total Tests: 728
Passed: 660
Failed: 68
Skipped: 0
Percent Passed: 90.66
Percent Failed: 9.34
Percent Skipped: 0.00
```
`$ ./senjink -url=http://example.com:8080 -search=[something in the jobname] -detail`
```
#!
Checking job dtests-jamq-700-candidate-acceptance-tests-r6i 
------------------------
jobname @ ibm_1.7.1
Passed: 163
Failed: 16
Skipped: 0
Percent Passed: 91.06
Percent Failed: 8.94
Percent Skipped: 0.00
------------------------
jobname @ ibm_1.8.0
Passed: 163
Failed: 16
Skipped: 0
Percent Passed: 91.06
Percent Failed: 8.94
Percent Skipped: 0.00
------------------------
jobname @ openjdk_1.7.0
Passed: 170
Failed: 21
Skipped: 0
Percent Passed: 89.01
Percent Failed: 10.99
Percent Skipped: 0.00
------------------------
jobname @ oracle_1.7.0
Passed: 164
Failed: 15
Skipped: 0
Percent Passed: 91.62
Percent Failed: 8.38
Percent Skipped: 0.00
========================
Total Tests: 728
Passed: 660
Failed: 68
Skipped: 0
Percent Passed: 90.66
Percent Failed: 9.34
Percent Skipped: 0.00
```