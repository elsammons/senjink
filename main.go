package main

import (
	"flag"
	"fmt"
	"strings"

	"bitbucket.org/elsammons/gojenkins"
	"bitbucket.org/elsammons/senjink/actions"
	"gopkg.in/cheggaaa/pb.v1"
)

/*
 Variables
*/
var (
	layout         = "2006-01-02"
	url            string
	jobNames       string
	search         string
	startDate      string
	endDate        string
	buildNumber    int64
	detail         bool
	bar            int
	totalPassCount int
	totalFailCount int
	totalSkipCount int
)

/*
 Store specific details for test results.
*/
type testResults struct {
	buildNumber    int64
	buildDate      string
	jobName        string
	jvm            string
	testsPassed    float64
	testsFailed    float64
	testsSkipped   float64
	totalTests     float64
	percentPassed  float64
	percentFailed  float64
	percentSkipped float64
}

/*
Initialize our command line options.
*/
func init() {

	flag.StringVar(&url, "url", "https://jenkins.mono-project.com/",
		"url of the jenkins server")
	flag.StringVar(&jobNames, "jobnames", "",
		"The specific job you are interested in")
	flag.Int64Var(&buildNumber, "build", 0,
		"A specific build you are interested in.")
	flag.StringVar(&search, "search", "mono",
		"Search for jobs that contain this string")
	flag.BoolVar(&detail, "detail", false, "Do you want a detailed view.")
	flag.IntVar(&bar, "bar", 0,
		"The total number of tests that had to run to consider worth counting.")

	flag.Parse()

	if search != "" && jobNames != "" {
		panic("search and jobNames are mutually exclusive.")
	}
}

func main() {
	/*
	   Initialize slice for holding jobs and
	   the jenkins connection.
	*/
	updatedJobs := make([]string, 0, 1000)
	jenkins := gojenkins.CreateJenkins(url)
	_, err := jenkins.Init()

	if err != nil {
		panic(err)
	}

	jobs, _ := jenkins.GetAllJobNames()
	/*
	   Find jobs based on search criteria.
	*/
	if search != "" {
		updatedJobs = actions.SearchJobsByName(jobs, search)
	} else if jobNames != "" {
		updatedJobs = strings.Split(jobNames, ",")
	}
	/*
	   Process updatedJobs and report.
	*/
	processJobs(jenkins, updatedJobs)
}

func processJobs(jenkins *gojenkins.Jenkins, updatedJobs []string) {
	resultSets := []*testResults{}
	results := new(testResults)
	count := len(updatedJobs)

	pBar := pb.StartNew(count)
	for _, j := range updatedJobs {
		job, err := jenkins.GetJob(j)
		if err != nil {
			fmt.Println("We encountered an error in updatedJobs, ", err)
			continue
		}
		/*
			if detail {
				fmt.Printf("Checking job %s \n", job.GetName())
			}
		*/
		/*
			Retrieve builds based on job
		*/
		build, err := job.GetLastCompletedBuild()
		if err != nil || build.IsRunning() {
			continue
		}
		/*
			Get Matrix runs from builds that are within date range.
		*/
		matrixRuns, _ := build.GetMatrixRuns()

		for _, run := range matrixRuns {
			resultSet, _ := run.GetResultSet()

			tests := []float64{float64(resultSet.PassCount),
				float64(resultSet.FailCount),
				float64(resultSet.SkipCount)}
			totalTests := actions.Sum(tests)
			/*
			   Check if the build is worth consideration.
			   totalTests must be greater than bar to count.
			*/
			if run.IsRunning() || int(totalTests) <= bar {
				continue
			}

			results = new(testResults)

			url := run.GetUrl()
			jvmTypeParam := strings.Split(url, ",")[1]
			jvmName := strings.Split(jvmTypeParam, "=")[1]

			results.buildNumber = build.GetBuildNumber()
			results.buildDate = build.GetTimestamp().Format(layout)
			results.jobName = j
			results.jvm = jvmName
			results.testsPassed = float64(resultSet.PassCount)
			results.testsFailed = float64(resultSet.FailCount)
			results.testsSkipped = float64(resultSet.SkipCount)
			results.totalTests = totalTests
			/*
			 * Calculate the Percentage for each.
			 */
			results.percentPassed = actions.CalculatePercent(results.testsPassed, results.totalTests)
			results.percentFailed = actions.CalculatePercent(results.testsFailed, results.totalTests)
			results.percentSkipped = actions.CalculatePercent(results.testsSkipped, results.totalTests)
			resultSets = append(resultSets, results)
		}
		pBar.Increment()
	}
	pBar.Finish()
	printReport(resultSets)
}

func printReport(resultSets []*testResults) {
	allPassed := make([]float64, 0, 1000)
	allFailed := make([]float64, 0, 1000)
	allSkipped := make([]float64, 0, 1000)
	for i := range resultSets {
		result := resultSets[i]
		if detail {
			fmt.Println(strings.Repeat("-", 24))
			fmt.Printf("%s @ %s\nPassed: %d\nFailed: %d\nSkipped: %d\nPercent Passed: %.2f\nPercent Failed: %.2f\nPercent Skipped: %.2f\n",
				result.jobName, result.jvm, int64(result.testsPassed), int64(result.testsFailed),
				int64(result.testsSkipped), result.percentPassed, result.percentFailed, result.percentSkipped)

		}
		allPassed = append(allPassed, result.testsPassed)
		allFailed = append(allFailed, result.testsFailed)
		allSkipped = append(allSkipped, result.testsSkipped)

	}

	totalAllPassed := actions.Sum(allPassed)
	totalAllFailed := actions.Sum(allFailed)
	totalAllSkipped := actions.Sum(allSkipped)

	allTotal := []float64{totalAllPassed, totalAllFailed, totalAllSkipped}
	allTotalTests := actions.Sum(allTotal)
	fmt.Println("*** Totals ***")
	fmt.Println(strings.Repeat("=", 24))
	fmt.Printf("Total Tests: %d\nPassed: %d\nFailed: %d\nSkipped: %d\nPercent Passed: %.2f\nPercent Failed: %.2f\nPercent Skipped: %.2f\n",
		int64(allTotalTests), int64(totalAllPassed), int64(totalAllFailed),
		int64(totalAllSkipped), actions.CalculatePercent(totalAllPassed, allTotalTests),
		actions.CalculatePercent(totalAllFailed, allTotalTests),
		actions.CalculatePercent(totalAllSkipped, allTotalTests))
}
